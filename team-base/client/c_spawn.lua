local firstSpawn = true

AddEventHandler("onClientMapStart", function()
    if firstSpawn then
        TriggerServerEvent("playerReady")
        firstSpawn = false
    end
end)