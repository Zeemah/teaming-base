-- what's this?
local tempTeam

-- set convar for constant logging
AddEventHandler("onResourceStart", function(resourceName)
    if resourceName ~= GetCurrentResourceName() then return end

    if forceDebug then
        SetConvar("teams.debugEnabled", "true")
    end
end)

-- a command to change team
RegisterCommand("team", function(source, args)
    local foundTeam = nil

    -- should probably fucking os.exit() this shit since if it happens you've really fucked shit up
    if not activeTeam[source] then DebugPrint(source, "No ID for current team, something went wrong...") return false end
    if not tonumber(args[1]) then DebugPrint(source, "Not a valid ID for a team - 1.") return false end

    local currentTeam = getMasterTeam(source)
    local foundTeam = getMasterTeam(source, tonumber(args[1]))

    if not tonumber(args[1]) or not foundTeam then DebugPrint(source, "Not a valid ID for a team - 3.") return false end
    if foundTeam.id == activeTeam[source] then 
        DebugPrint(source, "Tried switching to current team..?") 
        sendChatMessage(source, "[ERR] You are already on that team!", {221, 55, 55})
        return
    end
    
    print(typeof(source))
    for k, v in pairs(currentTeam.players) do
        print(typeof(v) .. " " .. v)
    end
    table.remove(currentTeam.players, source)
    table.insert(foundTeam.players, source)
    activeTeam[source] = foundTeam.id

    -- the following is radio stuff, so we ignore if they don't have radio

    if foundTeam.hasRadio then
        -- join message for new team
        for _, player in ipairs(foundTeam.players) do
            -- we check this to ensure the player actuall exists. save calling GetPlayerName() and checking if tonumber too
            if GetPlayers()[player] then
                sendChatMessage(player, string.format("[TC][%s] Player %s has joined your team!", string.upper(foundTeam.cmdName), GetPlayerName(source) .. " | " .. source), {radio_col_r, radio_col_g, radio_col_b})
            end
        end
    end

    if currentTeam.hasRadio then
        for _, player in ipairs(currentTeam.players) do
            if GetPlayers()[player] then
                sendChatMessage(player, string.format("[TC][%s] Player %s has joined your team!", string.upper(currentTeam.cmdName), GetPlayerName(source) .. " | " .. source), {radio_col_r, radio_col_g, radio_col_b})
            end
        end
    end

    if activeTeam[source] == defaultTeam then
        sendChatMessage(-1, string.format("[T] Player %s is now offduty.", GetPlayerName(source) .. " | " .. source))
    end
    
end, false)

RegisterCommand("teams", function(source)
    local thisColor = {86, 143, 213}
    local tNames = {}
    sendChatMessage(source, "Here is a list of all teams:", thisColor)
    for index, team in pairs(teams) do
        table.insert(tNames, team.longName .. " (" .. string.upper(team.cmdName) .. ")")
    end
    sendChatMessage(source, table.concat(tNames, "\n"), thisColor, true)
end)

RegisterCommand("reset_a", function(source)
    activeTeam[source] = defaultTeam
    local defTeam = getMasterTeam(source, defaultTeam)
    for _, player in pairs(GetPlayers()) do
        player = tonumber(player)
        activeTeam[player] = defaultTeam
        table.insert(defTeam.players, player)
    end

    for _, team in pairs(teams) do
        if team.id ~= defaultTeam then
            team.players = {
                [0] = 0
            }
        end
    end
    DebugPrint(source, "^1Reset all team data!")
end, true)