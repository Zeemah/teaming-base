RegisterNetEvent("playerReady")
AddEventHandler("playerReady", function()
    local tries = 1
    local source = source
    while defaultTeam == nil do
        Citizen.Wait(100)
        tries = tries + 1
        DebugPrint(source, "Could not find ID for the default team, trying again (" .. tries .. "/15)", true)
        if tries == 15 then
            DebugPrint(source, "Could not find ID for default team after 15 attempts, ^5return^3ing...", true)
            return
        end
    end

    local foundTeam
    for k, v in pairs(teams) do
        if v.id == defaultTeam then
            foundTeam = v
            break
        end
    end

    if type(foundTeam) ~= "table" then
        DebugPrint(source, "Something went wrong...")
        return
    end

    activeTeam[source] = foundTeam.id
    table.insert(foundTeam.players, source)

    DebugPrint(source, "Client loaded into the server and server ID inserted into the civilian team player list.")
end)