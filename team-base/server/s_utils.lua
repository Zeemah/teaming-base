-- this function gets the updated team from the 'main' team list so players and such are up-to-date
function getMasterTeam(player, id)
    local player = player or 0

    -- if no tid, something is definitely wrong
    if activeTeam[player] == nil then return false end
    
    -- you can also pass an argument to this function and get the update info for a different team
    -- this is used in the team changing command
    local id = id or activeTeam[player]    
    local currentMasterTeam = nil

    for k, v in pairs(teams) do
        if v.id == id then
            currentMasterTeam = v
            break
        end
    end
    return currentMasterTeam or false
end

-- dev command to force log _everything_
RegisterCommand("forceLog", function(source)
    debugging[source] = not debugging[source]

    if debugging[source] then
        DebugPrint(source, "Debug logs will always show now...")
    else
        sendChatMessage(source, "Disabled debug logs.")
    end
end)

-- a debug function with unnecessary parameters :^)
function DebugPrint(ply, message, force)
    -- a convar for always logging
    local forceDebugging = GetConvar("teams.debugEnabled", "false") == "true" and true or false

    -- don't log if on console or no player
    if ply == 0 then ply = nil end

    -- 'force' is a param which will always log stuff
    if force == nil then
        -- if player has always debug enabled, always debug...
        if debugging[ply] ~= nil then
            force = debugging[ply]
        else
            force = false
        end
    end

    if (force or forceDebugging) and ply ~= nil then
        TriggerClientEvent("clientPrint", ply, string.format("^4[%s]^3 %s", 
            string.upper(GetCurrentResourceName()), 
            (tostring(message) == "nil" and "A nil message was sent..." or tostring(message)))
        )
        return true
    end

    print(string.format("^4[%s]^3 %s", string.upper(GetCurrentResourceName()), tostring(message) == "nil" and "A nil message was sent..." or tostring(message)))
    return true
end

-- a chat message command to save having to use a table/array
function sendChatMessage(player, message, color, multiline)
    local player = player or 0
    local color = type(color) == 'table' and color or {255, 255, 255}

    -- ignore console and invalid players
    if player == 0 or not GetPlayers()[player] then return false end
    -- ignore nil message, duh
    if message == nil then return false end

    TriggerClientEvent("chat:addMessage", player, {
        args = { message },
        color = color,
        multiline = multiline == nil and false or multiline
    })

    return true
end

typeof = type