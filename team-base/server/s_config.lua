-- the id of the default team
defaultTeam = 8
forceDebug = true

radio_col = {
    r = 118,
    g = 211,
    b = 118
}

teams = {
    civilian = {
        id = 8,
        access = {
            restricted = false
        },
        cmdName = "civ",
        longName = "Civilian",
        hasRadio = false,
        isLEO = false,
        players = {
            [0] = 0
        }
    },
    
    spetsnaz = {
        id = 7,
        access = {
            restricted = false
        },
        cmdName = "spets",
        longName = "Spetsnaz",
        hasRadio = true,
        isLEO = true,
        players = {
            [0] = 0
        }
    }
}

activeTeam = {
    [0] = 0
}

debugging = {
    [0] = 0
}