AddEventHandler("playerDropped", function()
    if activeTeam[source] == nil then return end
        
    local team = getMasterTeam(source)
    if team == nil or team.players == nil then return end

    activeTeam[source] = nil
    table.remove(team.players, source)
end)